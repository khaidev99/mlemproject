package Mlem.com.Common.Services;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import Mlem.com.Common.Entity.User;
import Mlem.com.Common.Repository.UserRepository;
import lombok.Data;

@Service
@Data
public class UserService {

	@Autowired
	private UserRepository repo;

	public void processOAuthPostLogin(User user) {
		User existUser = repo.getUserByEmail(user.getEmail(), user.getProvider());
		System.out.print("find user" + existUser);

		if (existUser == null) {

			repo.save(user);

		}

	}
	
	public Optional<User> getUseById(Long id) {
		return repo.findById(id);
	}
	
	public void deleteUser(Long id) {
		repo.deleteById(id);
	}
	
	public User updateEnableUser(Long id,boolean enable) {
		User user = repo.findById(id).get();
		user.setEnable(enable);
		return repo.save(user);
	}
	
	public User updateRoleUser(Long id,int roleId) {
		User user = repo.findById(id).get();
		user.setRole(roleId);
		return repo.save(user);
	}

	public User getMyUserCookie(String userCookie) {
		User user =null;
		try {
			 Gson gson = new Gson();
			 user  = gson.fromJson(userCookie, User.class);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return user;
		}

	public void setMyUserCookie(User newUser, HttpServletResponse response) throws UnsupportedEncodingException {
		Gson gson = new Gson();	
		//User object to string
		String string = gson.toJson(newUser);
		Cookie newCookie = new Cookie("MY_USER", URLEncoder.encode(string, "utf-8"));					
        newCookie.setMaxAge(24 * 60 * 60);
        newCookie.setPath("/");
        response.addCookie(newCookie);
		
	}


}