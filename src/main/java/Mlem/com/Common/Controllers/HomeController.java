package Mlem.com.Common.Controllers;





import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Mlem.com.Common.Entity.User;
import Mlem.com.Common.Services.UserService;


 

@Controller
public class HomeController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping("/")
	public String Home(@CookieValue(value = "MY_USER", defaultValue = "defaultCookieValue") String userCookie,
			Model model) {
		if(userCookie.equals("defaultCookieValue")) {
			model.addAttribute("user", null);
		} 
		else {
			User user = userService.getMyUserCookie(userCookie);
			user.setFullName(user.getFullName().replace('+', ' '));
			model.addAttribute("user", user);
		}
		
	 return "index";
	}
	
	
}
